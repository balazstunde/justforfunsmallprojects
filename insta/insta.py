
'''
from instagram_web_api import Client, ClientCompatPatch, ClientError, ClientLoginError

# Without any authentication
web_api = Client(auto_patch=True, drop_incompat_keys=False)
user_feed_info = web_api.user_feed('329452045', count=10)
for post in user_feed_info:
    print('%s from %s' % (post['link'], post['user']['username']))
'''
'''
import instaloader
L = instaloader.Instaloader()
profile = instaloader.Profile.from_username(L.context, 'darai_andrea')
print(profile.get_posts)
'''

from instaloader import Instaloader, Profile

PROFILE = "nutriigirl"   # Insert profile name here

L = Instaloader()

# Obtain profile
profile = Profile.from_username(L.context, PROFILE)

# Get all posts and sort them by their number of likes
posts_sorted_by_likes = sorted(profile.get_posts(), key=lambda post: post.comments, reverse=True)

print(posts_sorted_by_likes[0])
# Download the post with the most likes
# L.download_post(posts_sorted_by_likes[0], PROFILE)