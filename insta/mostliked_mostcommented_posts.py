from instaloader import Instaloader, Profile

PROFILE = "dogs.r.cutie"   # Insert profile name here
L = Instaloader()

# Obtain profile
profile = Profile.from_username(L.context, PROFILE)
all_posts = profile.get_posts()

# Get all posts and sort them by their number of likes
posts_sorted_by_likes = sorted(all_posts, key=lambda post: post.likes, reverse=True)

# Get all posts and sort them by their number of likes
posts_sorted_by_comments = sorted(all_posts, key=lambda post: post.comments, reverse=True)

for post in posts_sorted_by_likes[:9]:
    print("https://instagram.com/p/" + post.shortcode + "/")
    print("Likes: " + str(post.likes))

# Download the post with the most likes
# L.download_post(posts_sorted_by_likes[0], PROFILE)

for post in posts_sorted_by_comments[:9]:
    print("https://instagram.com/p/" + post.shortcode + "/")
    print("Comments: " + str(post.comments))